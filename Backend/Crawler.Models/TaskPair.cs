﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InformationRetrievalService.Backend.Crawler.Models
{
    public class TaskPair
    {
        public Task Task { get; set; }
        public CancellationTokenSource CancelTokenSource { get; set; }
    }
}
