﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InformationRetrievalService.Backend.Crawler.Models.Indexing
{
    public class IndexingResultMetrics
    {
        public int TermCount { get; set; }
        public int LinksCount { get; set; }
    }
}
