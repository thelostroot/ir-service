﻿using System.Collections.Generic;
using InformationRetrievalService.Backend.Crawler.Models.Store.Entities;

namespace InformationRetrievalService.Backend.Crawler.Models.Indexing
{
    public class IndexingResult
    {
        public Document Document { get; set; }
        public List<IndexingDocumentTerm> DocumentTerms { get; set; }
        public List<Link> DocumentLinks { get; set; }
    }
}
