﻿namespace InformationRetrievalService.Backend.Crawler.Models.Indexing
{
    public class IndexingDocumentTerm
    {
        public string Name { get; set; }
        public int Freq { get; set; }

        public IndexingDocumentTerm(string name, int count)
        {
            Name = name;
            Freq = count;
        }
    }
}
