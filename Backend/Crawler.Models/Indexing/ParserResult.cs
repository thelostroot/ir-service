﻿using System.Collections.Generic;

namespace InformationRetrievalService.Backend.Crawler.Models.Indexing
{
    public class ParserResult
    {
        public string DocumentTitle { get; set; }
        public List<string> TextNodes { get; set; }
        public List<string> DocumentLinks { get; set; }

        public ParserResult(string documentTitle, List<string> textNodes, List<string> documentList)
        {
            DocumentTitle = documentTitle;
            TextNodes = textNodes;
            DocumentLinks = documentList;
        }
    }
}
