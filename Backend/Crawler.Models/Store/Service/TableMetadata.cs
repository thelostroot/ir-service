﻿namespace InformationRetrievalService.Backend.Crawler.Models.Store.Service
{
    public class TableMetadata
    {
        public long IdSeq;
        public long Count;
    }
}
