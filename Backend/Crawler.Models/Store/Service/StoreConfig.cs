﻿namespace InformationRetrievalService.Backend.Crawler.Models.Store.Service
{
    public class StoreConfig
    {
        private readonly string _basePath;
        private readonly string _terms;
        private readonly string _documentTerms;
        private readonly string _document;
        private readonly string _links;
        
        public string Terms => $"{_basePath}/{_terms}";
        public string DocumentTerms => $"{_basePath}/{_documentTerms}";
        public string Documents => $"{_basePath}/{_document}";
        public string Links => $"{_basePath}/{_links}";

        public StoreConfig(string basePath, string terms, string documentTerms, string document, string links)
        {
            _basePath = basePath;
            _terms = terms;
            _documentTerms = documentTerms;
            _document = document;
            _links = links;
        }
    }
}
