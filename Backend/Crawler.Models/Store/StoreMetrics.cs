﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InformationRetrievalService.Backend.Crawler.Models.Store
{
    public class StoreMetrics
    {
        public long TermsCount { get; set; }
        public long DocumentCount { get; set; }
        public long LinksCount { get; set; }
    }
}
