﻿namespace InformationRetrievalService.Backend.Crawler.Models.Store.Entities
{
    public class Term
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long DocumentFreq { get; set; }
    }
}
