﻿namespace InformationRetrievalService.Backend.Crawler.Models.Store.Entities
{
    public class Link
    {
        public long Id { get; set; }
        public string Url{get; set;}
    }
}
