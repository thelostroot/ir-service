﻿namespace InformationRetrievalService.Backend.Crawler.Models.Store.Entities
{
    public class DocumentTerm
    {
        public long TermId { get; set; }
        public double TermFreq { get; set; }
        public double TfIdf { get; set; }
    }
}
