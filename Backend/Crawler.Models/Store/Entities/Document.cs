﻿using System;
using System.Collections.Generic;

namespace InformationRetrievalService.Backend.Crawler.Models.Store.Entities
{
    public class Document
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string IndexingTime { get; set; }
        public List<DocumentTerm> Terms { get; set; }
    }
}
