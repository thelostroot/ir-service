﻿using InformationRetrievalService.Backend.Crawler.Models.Store.Entities;

namespace InformationRetrievalService.Backend.Crawler.Models.Store.Retrieval
{
    public class QueryResult
    {
        public Document Document { get; set; }
        public double Cos { get; set; }
    }
}
