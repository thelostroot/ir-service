﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using InformationRetrievalService.Backend.Crawler.Models.Store.Entities;
using InformationRetrievalService.Backend.Crawler.Models.Store.Service;
using InformationRetrievalService.Backend.Crawler.Store.BinaryStore.Serialization;
using InformationRetrievalService.Backend.RetrievalAlgorithms;

namespace InformationRetrievalService.Backend.Crawler.Store.BinaryStore
{
    public class BinaryStoreCore
    {
        public BinaryStoreCore() { }

        public void InitStore()
        {
            var metadata = new TableMetadata()
            {
                IdSeq = 1,
                Count = 0
            };

            var type = typeof(Term);
            WriteMetadata(SerializationMap.Files[type], metadata);

            type = typeof(Document);
            WriteMetadata(SerializationMap.Files[type], metadata);

            type = typeof(Link);
            WriteMetadata(SerializationMap.Files[type], metadata);
        }

        public void CleanTable<T>()
        {
            var metadata = new TableMetadata()
            {
                IdSeq = 1,
                Count = 0
            };

            var type = typeof(T);

            File.Delete(SerializationMap.Files[type]);

            WriteMetadata(SerializationMap.Files[type], metadata);
        }

        #region MetaData operations
        
        public TableMetadata ReadMetadata(string path)
        {
            using (BinaryReader reader = new BinaryReader(File.Open(path, FileMode.Open)))
            {
                var result = new TableMetadata()
                {
                    IdSeq = reader.ReadInt64(),
                    Count = reader.ReadInt64()
                };
                reader.Close();
                return result;
            }
        }

        public long GetCount<T>()
        {
            var type = typeof(T);
            using (BinaryReader reader = new BinaryReader(File.Open(SerializationMap.Files[type], FileMode.Open)))
            {
                reader.ReadInt64();
                return reader.ReadInt64();
            }
        }

        public void SkipMetadata(BinaryReader stream)
        {
            stream.BaseStream.Position = sizeof(long) + sizeof(long);
        }

        public void WriteMetadata(string path, TableMetadata tableMetadata)
        {
            using (BinaryWriter writer = new BinaryWriter(File.Open(path, FileMode.OpenOrCreate, FileAccess.Write)))
            {
                writer.Write(tableMetadata.IdSeq);
                writer.Write(tableMetadata.Count);
            }
        }
        #endregion;

        #region Selects
        public List<T> Select<T>(Func<T, bool> filter = null)
        {
            var type = typeof(T);
            var readerFunc = SerializationMap.Readers[type];

            var result = new List<T>();
            using (BinaryReader reader = new BinaryReader(File.Open(SerializationMap.Files[type], FileMode.Open)))
            {
                SkipMetadata(reader);

                while (reader.BaseStream.Position != reader.BaseStream.Length)
                {
                    var line = readerFunc(reader);
                    var tLine = (T)line;

                    if (filter != null)
                    {
                        if (filter(tLine)) result.Add(tLine);
                    }
                    else
                    {
                        result.Add(tLine);
                    }
                }

                reader.Close();
            }

            return result;
        }

        public List<T> ExistFilter<T>(List<T> entities, Func<T, bool> existPredicate)
        {
            var type = typeof(T);
            var readerFunc = SerializationMap.Readers[type];

            var existList = new List<T>();
            using (BinaryReader reader = new BinaryReader(File.Open(SerializationMap.Files[type], FileMode.Open)))
            {
                SkipMetadata(reader);

                while (reader.BaseStream.Position != reader.BaseStream.Length)
                {
                    var line = readerFunc(reader);
                    var tLine = (T)line;

                    if (existPredicate(tLine))
                    {
                        existList.Add(tLine);
                    }

                    if (existList.Count == entities.Count)
                        return existList;
                }

                reader.Close();
            }

            return existList;
        }
        #endregion

        #region Create
        public void Insert<T>(Document document)
        {
            var type = typeof(T);
            var writerFunc = SerializationMap.Writers[type];
            var metadata = ReadMetadata(SerializationMap.Files[type]);
            metadata.Count += 1;

            using (BinaryWriter writer = new BinaryWriter(File.Open(SerializationMap.Files[type], FileMode.Append, FileAccess.Write)))
            {
                document.GetType().GetProperty("Id")?.SetValue(document, metadata.IdSeq);
                writerFunc(writer, document);
                metadata.IdSeq++;
            }

            WriteMetadata(SerializationMap.Files[type], metadata);
        }

        public void Insert<T>(List<T> entities)
        {
            var type = typeof(T);
            var writerFunc = SerializationMap.Writers[type];
            var metadata = ReadMetadata(SerializationMap.Files[type]);
            metadata.Count += entities.Count;

            using (BinaryWriter writer = new BinaryWriter(File.Open(SerializationMap.Files[type], FileMode.Append, FileAccess.Write)))
            {
                foreach (var entity in entities)
                {
                    entity.GetType().GetProperty("Id")?.SetValue(entity, metadata.IdSeq);
                    writerFunc(writer, entity);
                    metadata.IdSeq++;
                }

                writer.Close();
            }

            WriteMetadata(SerializationMap.Files[type], metadata);
        }

        public void UpsertTerm(List<Term> terms)
        {
            var used = new List<Term>();
            var positions = new List<KeyValuePair<long, long>>();
            var type = typeof(Term);
            int dfValueSize = sizeof(long);
            using (BinaryReader reader = new BinaryReader(File.Open(SerializationMap.Files[type], FileMode.Open, FileAccess.Read)))
            {
                SkipMetadata(reader);
                while (reader.BaseStream.Position != reader.BaseStream.Length)
                {
                    // Считываем терм
                    var dbTerm = (Term)Serialization.EntityReaders.TermReader(reader);

                    // Проверяем есть ли он в переданном списке
                    var term = terms.FirstOrDefault(x => x.Name == dbTerm.Name);
                    if (term != null)
                    {
                        // Увеличиваем df на 1
                        dbTerm.DocumentFreq += 1;

                        // Запоминаем id
                        term.Id = dbTerm.Id;
                        term.DocumentFreq = dbTerm.DocumentFreq;

                        // Запоминаем позицию df что бы потом переписать
                        var dfPosition = reader.BaseStream.Position - dfValueSize;
                        positions.Add(new KeyValuePair<long, long>(dfPosition, dbTerm.DocumentFreq));

                        // Запоминаем терм как обработанный
                        used.Add(dbTerm);

                        // Проверяем обработали ли все термы
                        if (used.Count == terms.Count) break;
                    }
                }

                reader.Close();
            }

            // Обновляет df по сохраненным позициям
            if (positions.Count > 0)
            {
                using (BinaryWriter writer = new BinaryWriter(File.Open(SerializationMap.Files[type], FileMode.Open, FileAccess.Write)))
                {
                    foreach (var position in positions)
                    {
                        writer.BaseStream.Position = position.Key;
                        writer.Write(position.Value);
                    }

                    writer.Close();
                }
            }

            // Вставляем новые термы
            var newTerms = terms.Where(x => !used.Any(y => y.Name == x.Name)).ToList();
            if (newTerms.Count > 0)
                Insert<Term>(newTerms);
        }
        #endregion

        public void RebalanceWeights()
        {
            var type = typeof(Document);
            var readerFunc = SerializationMap.Readers[type];

            long totalDocumentCount = GetCount<Document>();

            var docs = new List<Document>();

            using (BinaryReader reader = new BinaryReader(File.Open(SerializationMap.Files[type], FileMode.Open)))
            {
                SkipMetadata(reader);

                while (reader.BaseStream.Position != reader.BaseStream.Length)
                {
                    // Читаем документ
                    var line = readerFunc(reader);
                    var document = (Document) line;

                    // Достаем термы документа
                    var documentTerms = document.Terms.Select(x => new Term()
                    {
                        Id = x.TermId
                    }).ToList();

                    // Ищем термы в БД
                    var dbTerms = ExistFilter<Term>(documentTerms, x => documentTerms.Any(term => term.Id == x.Id) );

                    // Вычисление веса для каждого терма
                    foreach (var term in document.Terms)
                    {
                        long df = dbTerms.First(x => x.Id == term.TermId).DocumentFreq;
                        term.TfIdf = VectorModel.GetTfIdf(term.TermFreq, df, totalDocumentCount);
                    }

                    // TODO костыль для перезаписи. Сохранять позицию
                    docs.Add(document);

                    Trace.WriteLine(docs.Count);
                }
            }

            // TODO костыль для перезаписи. Сохранять позицию
            CleanTable<Document>();
            Insert<Document>(docs);
        }

    }
}
