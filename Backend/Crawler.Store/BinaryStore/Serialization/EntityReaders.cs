﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using InformationRetrievalService.Backend.Crawler.Models.Store.Entities;

namespace InformationRetrievalService.Backend.Crawler.Store.BinaryStore.Serialization
{
    public static class EntityReaders
    {
        public static Func<BinaryReader, object> TermReader = reader => new Term()
        {
            Id = reader.ReadInt64(),
            Name = reader.ReadString(),
            DocumentFreq = reader.ReadInt64()
        };

        public static Func<BinaryReader, object> DocumentReader = reader =>
        {
            var document = new Document()
            {
                Id = reader.ReadInt64(),
                Title = reader.ReadString(),
                Url = reader.ReadString(),
                IndexingTime = reader.ReadString(),
                Terms = new List<DocumentTerm>()
            };

            int termCount = reader.ReadInt32();
            while (termCount != 0)
            {
                document.Terms.Add(new DocumentTerm()
                {
                    TermId = reader.ReadInt64(),
                    TermFreq = reader.ReadDouble(),
                    TfIdf = reader.ReadDouble()
                });
                termCount--;
            }

            return document;
        };

        public static Func<BinaryReader, object> LinkReader = reader => new Link()
        {
            Id = reader.ReadInt64(),
            Url = reader.ReadString()
        };
    }
}
