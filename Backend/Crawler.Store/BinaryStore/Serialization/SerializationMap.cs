﻿using System;
using System.Collections.Generic;
using System.IO;
using InformationRetrievalService.Backend.Crawler.Models.Store.Entities;

namespace InformationRetrievalService.Backend.Crawler.Store.BinaryStore.Serialization
{
    public static class SerializationMap
    {
        public static Dictionary<Type, Func<BinaryReader, object>> Readers;
        public static Dictionary<Type, Action<BinaryWriter, object>> Writers;
        public static Dictionary<Type, string> Files;

        static SerializationMap()
        {
            Readers = new Dictionary<Type, Func<BinaryReader, object>>
            {
                {typeof(Term), Serialization.EntityReaders.TermReader},
                {typeof(Document), Serialization.EntityReaders.DocumentReader},
                {typeof(Link), Serialization.EntityReaders.LinkReader }
            };

            Writers = new Dictionary<Type, Action<BinaryWriter, object>>
            {
                {typeof(Term), Serialization.EntityWriters.TermWriter},
                {typeof(Document), Serialization.EntityWriters.DocumentReader},
                {typeof(Link), Serialization.EntityWriters.LinkWriter }
            };

            Files = new Dictionary<Type, string> {{typeof(Term), "terms.dat"}, {typeof(Document), "documents.dat"}, { typeof(Link), "links.dat" } };
        }
    }
}
