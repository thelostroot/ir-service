﻿using System;
using System.Globalization;
using System.IO;
using InformationRetrievalService.Backend.Crawler.Models.Store.Entities;

namespace InformationRetrievalService.Backend.Crawler.Store.BinaryStore.Serialization
{
    public static class EntityWriters
    {
        public static Action<BinaryWriter, object> TermWriter = (writer,  termObject) =>
        {
            var term = (Term) termObject;
            writer.Write(term.Id);
            writer.Write(term.Name);
            writer.Write(term.DocumentFreq);
        };

        public static Action<BinaryWriter, object> DocumentReader = (writer, documentObject) =>
        {
            var document = (Document)documentObject;
            // Записываем данные документа
            writer.Write(document.Id);
            writer.Write(document.Title);
            writer.Write(document.Url);
            writer.Write(document.IndexingTime);

            // Количество термов
            writer.Write(document.Terms.Count);

            // Записываем термы
            foreach (var term in document.Terms)
            {
                writer.Write(term.TermId);
                writer.Write(term.TermFreq);
                writer.Write(term.TfIdf);
            }
        };

        public static Action<BinaryWriter, object> LinkWriter = (writer, linkObject) =>
        {
            var link = (Link) linkObject;
            writer.Write(link.Id);
            writer.Write(link.Url);
        };
    }
}
