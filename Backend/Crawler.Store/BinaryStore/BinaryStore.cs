﻿using System.Collections.Generic;
using System.Linq;
using InformationRetrievalService.Backend.Crawler.Models.Indexing;
using InformationRetrievalService.Backend.Crawler.Models.Store;
using InformationRetrievalService.Backend.Crawler.Models.Store.Entities;
using InformationRetrievalService.Backend.Crawler.Models.Store.Retrieval;
using InformationRetrievalService.Backend.RetrievalAlgorithms;
using InformationRetrievalService.Backend.Tokenizer;

namespace InformationRetrievalService.Backend.Crawler.Store.BinaryStore
{
    public class BinaryStore : IStore
    {
        private readonly BinaryStoreCore _store;

        public BinaryStore()
        {
            _store = new BinaryStoreCore();
        }

        public List<Link> GetSavedLinks()
        {
            return _store.Select<Link>();
        }

        public void SaveLinks(List<Link> links)
        {
            _store.CleanTable<Link>();
            _store.Insert<Link>(links);
        }

        public List<Document> GetDocumentsByUrl(List<Link> links)
        {
            var documents = links.Select(x => new Document()
            {
                Url = x.Url
            }).ToList();

            return _store.ExistFilter<Document>(documents, x =>  documents.Any(doc => doc.Url == x.Url));
        }

        public void SaveIndexedDocument(IndexingResult indexingResult)
        {
            /*
             * 1. Апсерт для входящих термов
             * 2. Вычисление списка термов для каждого документа
             *      2.1 Вычисление tf-idf для каждого терма. Зная актуальные df термов после апсерта
             * 3. Вставка документа
             */
            
            var terms = indexingResult.DocumentTerms.Select(x => new Term() { Name = x.Name, DocumentFreq = 1}).ToList();

            // Выполняем upsert он заполнит термам Id
            _store.UpsertTerm(terms);

            // Считаем общее количество термов в документе
            int totalTermsCount = indexingResult.DocumentTerms.Sum(x => x.Freq);

            // Получаем общее количество документов
            var totalDocumentCount = _store.GetCount<Document>();

            var totalCountInDocument = indexingResult.DocumentTerms.Sum(x => x.Freq);

            foreach (var documentTerm in indexingResult.DocumentTerms)
            {
                var dbTerm = terms.First(x => x.Name == documentTerm.Name);
                indexingResult.Document.Terms.Add(new DocumentTerm()
                {
                    TermId = dbTerm.Id,
                    TermFreq = VectorModel.GetTf(documentTerm.Freq, totalCountInDocument),
                    TfIdf = VectorModel.GetTfIdf(VectorModel.GetTf(documentTerm.Freq, totalTermsCount), dbTerm.DocumentFreq, totalDocumentCount)
                });
            }

            _store.Insert<Document>(indexingResult.Document);
        }

        public List<QueryResult> RankingQuery(string query)
        {
            /*
             * 1. Превратить запрос в вектор
             *      1.1 Разбить на токены
             *      1.2 Взвесить каждый токен
             *          1.2.1 Получить df всех токенов и вычислить idf для токенов
             *          1.2.2 Вычислить нормализировный tf по значениям из токенайзера
             * 2. Выбрать все документы с такими термами
             * 3. Отсортировать их по косинусной мере
             *
             */

            // Разбиваем на токены
            var tokenizer = new TextTokenizer();
            var tokens = tokenizer.DivideToTokens(query);

            // Получаем tf токенов. Методом групировки одинаковых токенов
            var queryTerms = tokens.GroupBy(x => x)
                .Select(x => new IndexingDocumentTerm(x.Key, x.Count())).ToList();

            // Считаем общее количество термов в запросе
            var totalTermCountInQuery = tokens.Count;

            // Достаем из базы общее число документов
            var totalDocumentCount = _store.GetCount<Document>();

            // Получаем термины запроса из базы
            var dbTerms = _store.Select<Term>(x => queryTerms.Any(indexingTerm => indexingTerm.Name == x.Name) );

            // Создаем документ из запроса и считаем tf-idf для каждого терма
            var queryDocument = new Document() { Terms = new List<DocumentTerm>()};
            foreach (var queryTerm in queryTerms)
            {
                // Считаем tf терма в запросе
                double tf = VectorModel.GetTf(queryTerm.Freq, totalTermCountInQuery);

                // Пытаемся получить df терма из базы
                var dbTerm = dbTerms.FirstOrDefault(x => x.Name == queryTerm.Name);

                // Пропускаем терм если его нет в базе
                if (dbTerm == null) continue;

                double tfIdf = VectorModel.GetTfIdf(tf, dbTerm.DocumentFreq, totalDocumentCount);

                queryDocument.Terms.Add(new DocumentTerm()
                {
                    TermId = dbTerm.Id,
                    TermFreq = tf,
                    TfIdf = tfIdf
                });
            }

            // Выбираем документы которые содержат термы из запроса
            var resultDocuments = _store.Select<Document>(x =>
                x.Terms.Any(dTerm => queryDocument.Terms.Any(qTerm => qTerm.TermId == dTerm.TermId)));

            // Проходим по найденым документам и оставляем у них только термины из запроса учитывая порядок
            foreach (var resultDocument in resultDocuments)
            {
                // Список только для термов из запроса
                var newTermList = new List<DocumentTerm>();

                foreach (var queryTerm in queryDocument.Terms)
                {
                    // Пытаемся найти терм из запроса
                    var resultDocumentTerm = resultDocument.Terms.FirstOrDefault(x => x.TermId == queryTerm.TermId);

                    // Если нашли добавляем сохраняя порядок если не нашли ставим 0
                    if (resultDocumentTerm != null)
                        newTermList.Add(resultDocumentTerm);
                    else
                        newTermList.Add(new DocumentTerm(){ TfIdf = 0});
                }

                resultDocument.Terms = newTermList;
            }

            // Считаем косинусную меру между документами
           return VectorModel.CosineSimilarity(queryDocument, resultDocuments).OrderByDescending(x => x.Cos).ToList();
        }

        public void RebalanceWeights()
        {
            _store.RebalanceWeights();
        }

        public StoreMetrics GetStoreMetrics()
        {
            return new StoreMetrics()
            {
                TermsCount = _store.GetCount<Term>(),
                DocumentCount = _store.GetCount<Document>(),
                LinksCount = _store.GetCount<Link>()
            };
        }

    }
}
