﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InformationRetrievalService.Backend.Crawler.Models.Indexing;
using InformationRetrievalService.Backend.Crawler.Models.Store;
using InformationRetrievalService.Backend.Crawler.Models.Store.Entities;
using InformationRetrievalService.Backend.Crawler.Models.Store.Retrieval;

namespace InformationRetrievalService.Backend.Crawler.Store
{
    public interface IStore
    {
        List<Link> GetSavedLinks();
        void SaveLinks(List<Link> links);
        List<Document> GetDocumentsByUrl(List<Link> links);
        void SaveIndexedDocument(IndexingResult indexingResult);
        List<QueryResult> RankingQuery(string query);
        StoreMetrics GetStoreMetrics();
    }
}
