﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InformationRetrievalService.Backend.Crawler.Models.Store;
using Npgsql;

namespace InformationRetrievalService.Backend.Crawler.Store.Postgresql
{
   /* public class PostgresqlProvider
    {
        private readonly NpgsqlConnection _connection;
        private readonly PostgresqlEntityAdapter _entityAdapter;
        private readonly PostgresqlProviderHelper _buildQueryHelper;
        public PostgresqlProvider(string connectionString, PostgresqlEntityAdapter entityAdapter)
        {
            _connection = new NpgsqlConnection(connectionString);
            _buildQueryHelper = new PostgresqlProviderHelper();
            _entityAdapter = entityAdapter;

            //_connection.Open();
        }

        ~PostgresqlProvider()
        {
            _connection.Close();
        }

        public List<Link> GetLinksPool()
        {
            string q = $"SELECT * FROM {_entityAdapter.LinksPoolTable}";
            using (var cmd = new NpgsqlCommand(q, _connection))
            {

                try
                {
                     _connection.Open();
                    using (var dr = cmd.ExecuteReader())
                    {
                       
                        var result = new List<Link>();
                        while (dr.Read())
                        {
                            result.Add(new Link()
                            {
                                Id = dr.GetInt64(0),
                                Address = dr.GetString(1)
                            });
                        }

                        return result;
                    }
                }
                catch (Exception e)
                {
                    throw new Exception($"Ошибка получение пула ссылок {e.Message}!");
                }
                finally
                {
                    _connection.Close();
                }
            }
        }

        public bool CheckDocumentExist(string url)
        {
            string q = $"SELECT * FROM {_entityAdapter.DocumentsTable} WHERE url='{url}'";
            using (var cmd = new NpgsqlCommand(q, _connection))
            {
                try
                {
                     _connection.Open();
                    using (var dr = cmd.ExecuteReader())
                    {
                        return dr.Read();
                    }
                }
                catch (Exception e)
                {
                    throw new Exception($"Ошибка при проверке существования документа {e.Message}!");
                }
                finally
                {
                     _connection.Close();
                }
            }
        }

        public bool CheckPoolLinkExist(string url)
        {
            string q = $"SELECT * FROM {_entityAdapter.LinksPoolTable} WHERE link_addr='{url}'";
            using (var cmd = new NpgsqlCommand(q, _connection))
            {
                try
                {
                    _connection.Open();
                    using (var dr = cmd.ExecuteReader())
                    {
                        return dr.Read();
                    }
                }
                catch (Exception e)
                {
                    throw new Exception($"Ошибка при проверке существования документа {e.Message}!");
                }
                finally
                {
                    _connection.Close();
                }
            }
        }

        public List<Term> GetTerms(List<string> names)
        {
            using (var cmd = _buildQueryHelper.BuildSelectTermsCommand(_connection, _entityAdapter.TermsTable, names))
            {
                try
                {
                    _connection.Open();
                    var result = new List<Term>();

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            result.Add(new Term()
                            {
                                Id = dr.GetInt64(0),
                                Name = dr.GetString(1),
                                DocumentFreq = dr.GetInt64(2)
                            });
                        }

                        return result;
                    }
                }
                catch (Exception e)
                {
                    throw new Exception($"Ошибка при проверке существования документа {e.Message}!");
                }
                finally
                {
                    _connection.Close();
                }
            }
        }

        public List<QueryResultProxy> SelectDocuments(List<string> terms)
        {
            using (var cmd = _buildQueryHelper.BuildSelectDocumentsCommand(_connection, _entityAdapter.TermsTable, terms))
            {
                try
                {
                    _connection.Open();
                    var result = new List<QueryResultProxy>();

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            result.Add(new QueryResultProxy()
                            {
                                DocumentId = dr.GetInt64(0),
                                DocumentTitle = dr.GetString(1),
                                DocumentUrl = dr.GetString(2),
                                Term = dr.GetString(3),
                                TfIdf = dr.GetDouble(4)
                            });
                        }

                        return result;
                    }
                }
                catch (Exception e)
                {
                    throw new Exception($"Ошибка при выборке документов {e.Message}!");
                }
                finally
                {
                    _connection.Close();
                }
            }
        }

        public void InsertDocumentTerm(DocumentTerms documentTerm)
        {
            Insert(_entityAdapter.DocumentTermsTable, _entityAdapter.GetDocumentTermFields(documentTerm));
        }

        public Document InsertDocument(Document document)
        {
            var id = Insert(_entityAdapter.DocumentsTable, _entityAdapter.GetDocumentFields(document));
            document.Id = id;
            return document;
        }

        public void SaveLinksPool(List<Link> links)
        {
            ClearPoolLink();
            foreach (var link in links)
            {
                Insert(_entityAdapter.LinksPoolTable, _entityAdapter.GetPoolLinkFields(link));
            }
        }

        private long Insert(string tableName, List<KeyValuePair<string, object>> entityFields)
        {
            using (var cmd = _buildQueryHelper.BuildInsertCommand(_connection, tableName, entityFields))
            {
                try
                {
                    _connection.Open();
                    using (var dr = cmd.ExecuteReader())
                    {
                        dr.Read();
                        return dr.GetInt64(0);
                    }
                }
                catch (Exception e)
                {
                    throw new Exception($"Ошибка при вставке! {e.Message}");
                }
                finally
                {
                    _connection.Close();
                }
            }
        }

        public void Update(string tableName, List<KeyValuePair<string, object>> entityFields)
        {
            using (var cmd = _buildQueryHelper.BuildUpdateCommand(_connection, tableName, entityFields))
            {
                try
                {
                    _connection.Open();
                    cmd.ExecuteReader();
                }
                catch (Exception e)
                {
                    throw new Exception($"Ошибка при обновлении! {e.Message}");
                }
                finally
                {
                    _connection.Close();
                }
            }
        }

        public void DeleteById(string tableName, long id)
        {
            using (var cmd = _buildQueryHelper.BuildDeleteCommand(_connection, tableName, id))
            {
                try
                {
                    // _connection.Open();
                    cmd.ExecuteReader();
                }
                catch (Exception e)
                {
                    throw new Exception($"Ошибка при удалении! {e.Message}");
                }
                finally
                {
                    // _connection.Close();
                }
            }
        }

        public Term UpsertTerm(Term term)
        {
            string q = $"INSERT INTO {_entityAdapter.TermsTable} AS t (name, df) values ('{term.Name}', 1) ON CONFLICT(name) DO UPDATE SET df = t.df + 1 RETURNING t.id, t.name, t.df";

            using (var cmd = new NpgsqlCommand(q, _connection))
            {
                try
                {
                     _connection.Open();
                    using (var dr = cmd.ExecuteReader())
                    {
                        dr.Read();
                        long id = dr.GetInt64(0);
                        string name = dr.GetString(1);
                        long freq = dr.GetInt64(2);
                        return new Term()
                        {
                            Id = id,
                            Name = name,
                            DocumentFreq = freq
                        };
                    }
                }
                catch(Exception e)
                {
                    throw new Exception("Ошибка вставки термина!");
                }
                finally
                {
                    _connection.Close();
                }
            }
        }

        public long GetDocumentsCount()
        {
            string q = $"SELECT count(id) FROM {_entityAdapter.DocumentsTable}";
            using (var cmd = new NpgsqlCommand(q, _connection))
            {
                try
                {
                    _connection.Open();
                    using (var dr = cmd.ExecuteReader())
                    {
                        dr.Read();
                        long count = dr.GetInt64(0);
                        return count;
                    }
                }
                catch(Exception e)
                {
                    throw new Exception($"Ошибка подсчета количесвта документов {e.Message}!");
                }
                finally
                {
                    _connection.Close();
                }
            }
        }

        public void Rebalance()
        {

        }

        public void ClearPoolLink()
        {
            string q = $"DELETE FROM {_entityAdapter.LinksPoolTable}";
            using (var cmd = new NpgsqlCommand(q, _connection))
            {
                try
                {
                    _connection.Open();
                    cmd.ExecuteReader();
                }
                catch (Exception e)
                {
                    throw new Exception($"Ошибка очистки ссылок из пула {e.Message}");
                }
                finally
                {
                    _connection.Close();
                }
            }
        }
    }*/
}
