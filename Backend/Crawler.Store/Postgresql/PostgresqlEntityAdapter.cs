﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InformationRetrievalService.Backend.Crawler.Models;
using InformationRetrievalService.Backend.Crawler.Models.Store;

namespace InformationRetrievalService.Backend.Crawler.Store.Postgresql
{
  /*  public class PostgresqlEntityAdapter
    {
        public readonly string TermsTable = "terms";
        public readonly string DocumentsTable = "documents";
        public readonly string DocumentTermsTable = "document_terms";
        public readonly string LinksPoolTable = "links_pool";

        public List<KeyValuePair<string, object>> GetTermFields(Term term)
        {
            var result = new List<KeyValuePair<string, object>>
            {
                new KeyValuePair<string, object>("name", term.Name),
                new KeyValuePair<string, object>("df", term.DocumentFreq),
            };
            return result;
        }

        public List<KeyValuePair<string, object>> GetDocumentFields(Document document)
        {
            var result = new List<KeyValuePair<string, object>>
            {
                new KeyValuePair<string, object>("title", document.Title),
                new KeyValuePair<string, object>("url", document.Url),
                new KeyValuePair<string, object>("indexing_time", document.IndexingTime)
            };
            return result;
        }

        public List<KeyValuePair<string, object>> GetDocumentTermFields(DocumentTerms documentTerm)
        {
            var result = new List<KeyValuePair<string, object>>
            {
                new KeyValuePair<string, object>("term_id", documentTerm.TermId),
                new KeyValuePair<string, object>("document_id", documentTerm.DocumentId),
                new KeyValuePair<string, object>("tf", documentTerm.TermFreq),
                new KeyValuePair<string, object>("tf_idf", documentTerm.TfIdf)
            };
            return result;
        }

        public List<KeyValuePair<string, object>> GetPoolLinkFields(Link poolLink)
        {
            var result = new List<KeyValuePair<string, object>>
            {
                new KeyValuePair<string, object>("link_addr", poolLink.Address)
            };
            return result;
        }
    }*/
}
