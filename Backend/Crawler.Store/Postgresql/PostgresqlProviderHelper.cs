﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace InformationRetrievalService.Backend.Crawler.Store.Postgresql
{
  /*  public class PostgresqlProviderHelper
    {
        public NpgsqlCommand CreateCommandWithParameters(NpgsqlConnection connection, string q, List<KeyValuePair<string, object>> fields)
        {
            var cmd = new NpgsqlCommand(q, connection);
            foreach (var field in fields)
                cmd.Parameters.AddWithValue(field.Key, field.Value);

            return cmd;
        }

        public NpgsqlCommand BuildSelectTermsCommand(NpgsqlConnection connection, string tableName, List<string> names)
        {
            var preparedNames = names.Select(x => $"name='{x}'");
            string q = $"SELECT * FROM {tableName} WHERE {string.Join(" OR ", preparedNames)}";
            return new NpgsqlCommand(q, connection);
        }

        public NpgsqlCommand BuildSelectDocumentsCommand(NpgsqlConnection connection, string tableName, List<string> terms)
        {
            var q = $@"SELECT dt.document_id, d.title, d.url, t.name, dt.tf_idf FROM terms AS t
                          INNER JOIN document_terms AS dt ON dt.term_id = t.id
                            INNER JOIN documents AS d ON dt.document_id = d.id
                              WHERE ";
            var where = string.Join(" OR ", terms.Select(x => $"t.name='{x}'"));
            return new NpgsqlCommand($"{q} {where}", connection);
        }

        public NpgsqlCommand BuildInsertCommand(NpgsqlConnection connection, string tableName, List<KeyValuePair<string, object>> fields)
        {
            string q = $"INSERT INTO {tableName} ";

            string keyPart = string.Join(",", fields.Select(x => x.Key)); ;
            string valuePart = $"@{string.Join(",@", fields.Select(x => x.Key))}";

            q += $"({keyPart}) VALUES ({valuePart}) RETURNING id";

            return CreateCommandWithParameters(connection, q, fields);
        }

        public NpgsqlCommand BuildUpdateCommand(NpgsqlConnection connection, string tableName, List<KeyValuePair<string, object>> fields)
        {
            string q = $"UPDATE {tableName} SET ";

            foreach (var field in fields)
                q += $"{field.Key}=@{field.Key}";

            return CreateCommandWithParameters(connection, q, fields);
        }

        public NpgsqlCommand BuildDeleteCommand(NpgsqlConnection connection, string tableName, long id)
        {
            string q = $"DELETE FROM {tableName} WHERE id={id}";
            return new NpgsqlCommand(q, connection);
        }
    }*/
}
