﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using InformationRetrievalService.Backend.Crawler.Models;
using InformationRetrievalService.Backend.Crawler.Models.Indexing;
using InformationRetrievalService.Backend.Crawler.Models.Store;
using InformationRetrievalService.Backend.Crawler.Models.Store.Entities;
using Npgsql;

namespace InformationRetrievalService.Backend.Crawler.Store.Postgresql
{
    /*public sealed class PostgresqlStore : IStore
    {
        private readonly string _connectionString = "Server=localhost;Port=5432;User Id=postgres;Password=1;Database=IR";
        private readonly PostgresqlProvider _postgresqlStoreProvider;

        public PostgresqlStore()
        {
            var entityAdapter = new PostgresqlEntityAdapter();
            _postgresqlStoreProvider = new PostgresqlProvider(_connectionString, entityAdapter);
        }

        public List<Link> GetPoolLinks()
        {
            return _postgresqlStoreProvider.GetLinksPool();
        }

        public void SavePoolLinks(List<Link> linksPool)
        {
            _postgresqlStoreProvider.SaveLinksPool(linksPool);
        }

        public bool CheckDocumentExist(string url)
        {
            bool existInDocuments = _postgresqlStoreProvider.CheckDocumentExist(url);
            return existInDocuments ;
        }*/

        /*public void SaveIndexingDocument(IndexingResult indexingResult)
        {
            /*
             * 1. Добавить терм в таблицу термов
             *   - если существует обновить df = df + 1
             * INSERT INTO terms  AS t (name, df) values ('cat2', 1)
               ON CONFLICT (name) DO UPDATE SET df = t.df+1 returning t.df;

             * 2. Добавить документ в таблицу
             * 3. Подсчитать tf-idf зная df термина по таблице и tf из параметров
             * 4. Внести каждую связь документ-терм в таблицу document_terms
             */

            // Вставляем все термины
           /* var terms = new List<Term>();
            foreach (var term in indexingResult.DocumentTerms)
                terms.Add(_postgresqlStoreProvider.UpsertTerm(new Term() {Name = term.Name}));

            // Вставляем документ
            var document = _postgresqlStoreProvider.InsertDocument(indexingResult.Document);

            double tf = indexingResult.DocumentTerms.Sum(x => x.Freq);

            // Вычисляем значение tf-idf для терминов
            long N = _postgresqlStoreProvider.GetDocumentsCount();
            double logN = Math.Log(N);

            foreach (var term in terms)
            {
                var logDf = Math.Log(term.DocumentFreq);
                var idf = logN - logDf;
                double normalizedTf = indexingResult.DocumentTerms.First(x => x.Name == term.Name).Freq / tf;

                var tfIdf = tf * idf;

                var documentTerm = new DocumentTerms()
                {
                    TermId = term.Id,
                    DocumentId = document.Id,
                    TermFreq = normalizedTf,
                    TfIdf = tfIdf
                };
                _postgresqlStoreProvider.InsertDocumentTerm(documentTerm);
            }
        }
    /*
        public List<Term> GetTerms(List<string> terms)
        {
            return _postgresqlStoreProvider.GetTerms(terms);
        }
    }*/
}
