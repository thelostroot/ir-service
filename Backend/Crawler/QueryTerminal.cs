﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InformationRetrievalService.Backend.Crawler.Store;
using InformationRetrievalService.Backend.Crawler.Store.BinaryStore;

namespace InformationRetrievalService.Backend.Crawler
{
    public partial class QueryTerminal : Form
    {
        public QueryTerminal()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            IStore store = new BinaryStore();
            var result = store.RankingQuery(textBox1.Text);
            var resultList = result.Select(x => $"{x.Document.Title} \t {x.Cos}    {x.Document.Url}");
            richTextBox1.Text = string.Join(Environment.NewLine, resultList);
        }
    }
}
