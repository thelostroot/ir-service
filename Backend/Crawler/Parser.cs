﻿using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;
using InformationRetrievalService.Backend.Crawler.Models;
using InformationRetrievalService.Backend.Crawler.Models.Indexing;

namespace InformationRetrievalService.Backend.Crawler
{
    class Parser
    {
        private readonly List<string> _ignoreTagList;
        
        private HtmlAgilityPack.HtmlDocument _htmlDocument;        

        string __resultReport = "";
        string __linksReport = "";

        public Parser()
        {
            _htmlDocument = new HtmlAgilityPack.HtmlDocument();

            _ignoreTagList = new List<string> { "script", "iframe", "style", "air" };
        }       

        public ParserResult Parse(string response, string baseUrl)
        { 
            _htmlDocument.LoadHtml(response);

            // Получаем все ноды из документа
            var nodes = new Queue<HtmlNode>();
            var bodyNode = _htmlDocument.DocumentNode.SelectNodes("//body")[0];
            nodes.Enqueue(bodyNode);
            
            var textNodes = new List<string>();
            var links = new List<string>();
            while (nodes.Count > 0)
            {
                // Достаем первую ноду из очереди
                var node = nodes.Dequeue();

                // Кладем все ноды текущей ноды в очередь
                foreach (var childNode in node.ChildNodes)
                    nodes.Enqueue(childNode);

                // Пропускаем ноды из игнор списка
                if (_ignoreTagList.Contains(node.Name) || _ignoreTagList.Contains(node.ParentNode.Name))
                    continue;

                // Пропускаем пустые ноды
                if (string.IsNullOrWhiteSpace(node.InnerText) == true)
                    continue;

                // Сохраняем ссылку в список
                if(node.Name == "a" && node.Attributes["href"] != null)
                {
                    var link = PrepareLink(node.Attributes["href"].Value, baseUrl);
                    if(!links.Contains(link))
                        links.Add(link);

                    /*if(node.Attributes["href"].Value != link)
                        __linksReport += "TRANSFORM===== " + node.Attributes["href"].Value + " >>>>>>> " + link + Environment.NewLine;
                    else
                        __linksReport += link + Environment.NewLine;*/
                }

                // Берем текст ноды если эта нода конечная
                if (node.ChildNodes.Count == 0)                   
                    textNodes.Add(node.InnerText);                

            }

            //System.IO.File.WriteAllText(@"c:\out.txt", __resultReport);
            //System.IO.File.WriteAllText(@"c:\links.txt", __linksReport);

            var titleNode = bodyNode.SelectNodes("//title").First();
            string titleText = "";
            if (titleNode != null)
                titleText = titleNode.InnerText;

            return new ParserResult(titleText, textNodes, links);
        }

        public string PrepareLink(string link, string baseUrl)
        {
            // Отсекаем хеш ссылки
            int hashIndex = link.IndexOf("#");
            if (hashIndex != -1)
            {                
                link = link.Substring(0, hashIndex);                
            }

            // Формируем абсолютные ссылки из относительных
            if (link.IndexOf("://") == -1)
            {                                 
                link = baseUrl + link;
            }

            // Удаляем слеш в конце
            if (link[link.Length - 1] == '/')
                link = link.Substring(0, link.Length - 1);

            return link;
        }
    }
}
