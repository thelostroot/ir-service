﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Threading.Tasks;
using InformationRetrievalService.Backend.Crawler.Models.Store;
using InformationRetrievalService.Backend.Crawler.Models.Store.Entities;
using InformationRetrievalService.Backend.Crawler.PoolManager;
using InformationRetrievalService.Backend.Crawler.Store;

namespace InformationRetrievalService.Backend.Crawler.PoolManager
{
    public static class LinkPool
    {
       public static List<Link> Links;
        private static IStore _store;

        static LinkPool()
        {
            Links = new List<Link>();
        }

        public static void ConnectToStore(IStore storeManager)
        {
            _store = storeManager;
        }

        public static void LoadLinkPool()
        {
            Links.AddRange(_store.GetSavedLinks());
        }

        public static string GetLink()
        {
            if (Links.Count == 0) return null;

            var first = Links.First();
            Links.RemoveAt(0);
            return first.Url;
        }

        public static void SaveDocumentLinks(List<Link> documentLinks, Func<string, bool> filter)
        {
            // Узнаем какие URL из списка уже проиндексированы
            var existDocuments = _store.GetDocumentsByUrl(documentLinks);

            // Добавляем в пул ссылки которых нет ни в проиндексированых ни в пуле ссылок
            foreach (var link in documentLinks)
            {
                bool existInStore = existDocuments.Any(x => x.Url == link.Url);
                bool existInMemoryPool = Links.Any(x => x.Url == link.Url);
                bool filterResult = filter(link.Url);
                if (!existInStore && !existInMemoryPool && filterResult)
                    Links.Add(link);
            }
        }

        public static void SaveCurrentPool()
        {
            if(Links.Count == 0 ) return;
            _store.SaveLinks(Links);
        }


    }
}
