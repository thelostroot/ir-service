﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using InformationRetrievalService.Backend.Crawler.Stemmers;

namespace InformationRetrievalService.Backend.Tokenizer
{
   public class TextTokenizer
    {
        readonly IStemmer _russianStemmer;
        readonly IStemmer _englishStemmer;

        enum Languages {English, Russian}

        public TextTokenizer()
        {
            _russianStemmer = new RussianStemmer();
            _englishStemmer = new EnglishStemmer();
        }

        public List<string> DivideToTokens(string text)
        {
            return GetTokens(text);
        }

        public List<string> DivideToTokens(List<string> nodes)
        {
            var terms = new List<string>();
            foreach (var node in nodes)            
                terms.AddRange(GetTokens(node));

            return terms;
        }        

        private List<string> GetTokens(string text)
        {
            var russianTokens = new List<string>();
            var russianRegex = new Regex("[а-яА-Я]+");
            foreach (Match match in russianRegex.Matches(text))
                russianTokens.Add(_russianStemmer.Stem(match.Value));

            var englishTokens = new List<string>();
            var englishRegex = new Regex("[a-zA-Z]+");
            foreach (Match match in englishRegex.Matches(text))
                englishTokens.Add(_englishStemmer.Stem(match.Value));

            russianTokens.AddRange(englishTokens);
            return russianTokens;
        }
    }
}
