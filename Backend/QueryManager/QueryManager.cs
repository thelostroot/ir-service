﻿using System;
using InformationRetrievalService.Backend.Crawler.Models.Store;
using System.Collections.Generic;
using System.Linq;
using InformationRetrievalService.Backend.Crawler.Models.Indexing;
using Tokenizer;

namespace InformationRetrievalService.Backend.QueryManager
{
    public class QueryTerminal
    {
      /*  public List<Document> RankingQuery(string query)
        {
            /*
             * 1. Превратить запрос в вектор
             *      1.1 Разбить на токены
             *      1.2 Взвесить каждый токен
             *          1.2.1 Получить df всех токенов и вычислить idf для токенов
             *          1.2.2 Вычислить нормализировный tf по значениям из токенайзера
             * 2. Выбрать все документы с такими термами
             * 3. Отсортировать их по косинусной мере
             *
             */
        
        /*    var entityAdapter = new PostgresqlEntityAdapter();
            string connectionString = "Server=localhost;Port=5432;User Id=postgres;Password=1;Database=IR";
            var postgresqlStoreProvider = new PostgresqlProvider(connectionString, entityAdapter);

            var queryVectorModel = new DocumentVectorModel();

            // Разбиваем на токены
            var tokenizer = new TextTokenizer();
            var tokens = tokenizer.DivideToTokens(query);

            // Получаем tf токенов. Методом групировки одинковых токенов
            var queryTerms = tokens.GroupBy(x => x)
                .Select(x => new IndexingDocumentTerm(x.Key, x.Count())).ToList();

            // Получаем термины запроса из базы
            var store = new PostgresqlStore();
            var terms = store.GetTerms(queryTerms.Select(x => x.Name).ToList());

            // Вычисляем tf-idf для каждого терма
            double tf = tokens.Count;
            long N = postgresqlStoreProvider.GetDocumentsCount();
            double logN = Math.Log(N);
            foreach (var queryTerm in queryTerms)
            {
                var dbTerm = terms.First(x => x.Name == queryTerm.Name);
                var logDf = Math.Log(dbTerm.DocumentFreq);
                var idf = logN - logDf;
                double normalizedTf = queryTerm.Freq / tf;
                double tfIdf = idf * normalizedTf;
                queryVectorModel.Terms.Add(new VectorTerm()
                {
                    Id = dbTerm.Id,
                    Term = queryTerm.Name,
                    TfIdf = tfIdf
                });

            }

            // Выбираем все документы которые содержат термины из запроса
            var dbVectors = new List<DocumentVectorModel>();
            var documents = postgresqlStoreProvider.SelectDocuments(terms.Select(x => x.Name).ToList());
            var documentGroups = documents.GroupBy(x => x.DocumentId);
            

            foreach (var g in documentGroups)
            {
                var document = new DocumentVectorModel {DocumentId = g.Key, Url = g.First().DocumentUrl};
                foreach (var doc in g)
                {
                    document.Terms.Add(new VectorTerm()
                    {
                        Term = doc.Term,
                        TfIdf = doc.TfIdf
                    });
                }

                dbVectors.Add(document);
            }

            // Ранжирование
            CosineSimilarity(queryVectorModel, dbVectors);


            return null;
        }

        private void CosineSimilarity(DocumentVectorModel queryVectorModel, List<DocumentVectorModel> dbVectors)
        {
            var result = new List<QueryResult>();

            double queryVectorModelPow = queryVectorModel.Terms.Sum(x => Math.Pow(x.TfIdf, 2));
            foreach (var vector in dbVectors)
            {
                if(vector.Terms.Count != 2)
                    continue;
                
                double top = 0;
                foreach (var queryTerm in queryVectorModel.Terms)
                {
                    var dbTerm = vector.Terms.FirstOrDefault(x => x.Term == queryTerm.Term);
                    if(dbTerm == null)
                        continue;

                    double mul = queryTerm.TfIdf * dbTerm.TfIdf;
                    top += mul;
                }

                double vectorSum = vector.Terms.Sum(x => Math.Pow(x.TfIdf, 2));
                double bottom = Math.Sqrt(queryVectorModelPow) * Math.Sqrt(vectorSum);

                double cos = top / bottom;

                result.Add(new QueryResult()
                {
                    Url = vector.Url,
                    Cos = cos
                });
            }

            var sort = result.OrderBy(x => x.Cos);


        }*/
    }
}
