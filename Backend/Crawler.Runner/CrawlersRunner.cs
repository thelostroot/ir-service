﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using InformationRetrievalService.Backend.Crawler.Models;
using InformationRetrievalService.Backend.Crawler.Models.Indexing;
using InformationRetrievalService.Backend.Crawler.Models.Store.Entities;
using InformationRetrievalService.Backend.Crawler.PoolManager;
using InformationRetrievalService.Backend.Crawler.Store;
using InformationRetrievalService.Backend.Crawler.Store.BinaryStore;

namespace InformationRetrievalService.Backend.Crawler.Runner
{
    public class CrawlersRunner
    {
        private readonly IStore _store;
        private readonly List<TaskPair> _tasks;
        private readonly Action<string, IndexingResultMetrics> _writeLog;

        public CrawlersRunner(IStore store, Action<string, IndexingResultMetrics> writeLog)
        {
            _store = store;
            _tasks = new List<TaskPair>();
            _writeLog = writeLog;
        }

        public void Run(int count)
        {
            LinkPool.ConnectToStore(_store);
            LinkPool.LoadLinkPool();

            if (LinkPool.Links.Count == 0)
            {
                LinkPool.Links.Add(new Link()
                {
                    Url = "https://ru.wikipedia.org/wiki/%D0%A6%D0%B5%D0%BD%D1%82%D1%80%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D0%B9_%D0%BF%D1%80%D0%BE%D1%86%D0%B5%D1%81%D1%81%D0%BE%D1%80"
                });
            }
            
            for (int i = 0; i < count; i++)
            {
                var crawler = new Crawler(_store, _writeLog);

                CancellationTokenSource cancelTokenSource = new CancellationTokenSource();
                CancellationToken token = cancelTokenSource.Token;

                int index = i;
                _tasks.Add(new TaskPair()
                {
                    Task = Task.Run(() => crawler.Run(token, index.ToString())),
                    CancelTokenSource = cancelTokenSource
                });
            }
        }

        public void StopAll()
        {
            foreach (var task in _tasks)
            {
                task.CancelTokenSource.Cancel();
            }
        }
    }
}
