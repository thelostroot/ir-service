﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InformationRetrievalService.Backend.Crawler.Models.Store.Entities;
using InformationRetrievalService.Backend.Crawler.Store.BinaryStore;

namespace InformationRetrievalService.Backend.CrawlerAdmin
{
    public partial class StoreBrowser : Form
    {
        public StoreBrowser()
        {
            InitializeComponent();
        }

        private void CrearGrid()
        {
            grid.Rows.Clear();
            grid.Columns.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CrearGrid();
            var store = new BinaryStoreCore();
            var terms = store.Select<Term>();

            grid.Columns.Add("id", "Id");
            grid.Columns.Add("name", "Name");
            grid.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            grid.Columns.Add("df", "DocumentFreq");
            foreach (var term in terms)
            {
                grid.Rows.Add(term.Id, term.Name, term.DocumentFreq);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CrearGrid();
            var store = new BinaryStoreCore();
            var documents = store.Select<Document>();

            grid.Columns.Add("id", "Id");

            grid.Columns.Add("title", "Title");
            grid.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            grid.Columns.Add("url", "Url");
            grid.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            grid.Columns.Add("date", "IndexingTime");

            grid.Columns.Add("terms", "Terms");
            grid.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            string report="";
            foreach (var document in documents)
            {
                report += $"{document.Url} TERMS: {Environment.NewLine}";
                report += string.Join(" ", document.Terms.Select(x => $"[{x.TermId} {x.TermFreq} {x.TfIdf}]"));

                /*grid.Rows.Add(document.Id, document.Title, document.Url, document.IndexingTime,
                    string.Join("\t", document.Terms.Select(x => $"{x.TermId} {x.TermFreq} {x.TfIdf}")));*/

                grid.Rows.Add(document.Id, document.Title, document.Url, document.IndexingTime);

                report += Environment.NewLine + Environment.NewLine;
            }

            File.WriteAllText(@"C:\\report.txt", report);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            CrearGrid();
            var store = new BinaryStoreCore();
            var links = store.Select<Link>();

            grid.Columns.Add("id", "Id");
            grid.Columns.Add("url", "Url");
            grid.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            foreach (var link in links)
            {
                grid.Rows.Add(link.Id, link.Url);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string terms = "terms.dat";
            string documents = "documents.dat";
            string links = "links.dat";
            if (System.IO.File.Exists(terms)) System.IO.File.Delete(terms);
            if (System.IO.File.Exists(documents)) System.IO.File.Delete(documents);
            if (System.IO.File.Exists(links)) System.IO.File.Delete(links);
            var store = new BinaryStoreCore();
            store.InitStore();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var store = new BinaryStoreCore();
            store.RebalanceWeights();
        }
    }
}
