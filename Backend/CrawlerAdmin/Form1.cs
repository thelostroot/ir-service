﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using System.Threading.Tasks;
using InformationRetrievalService.Backend.Crawler;
using InformationRetrievalService.Backend.Crawler.Models;
using InformationRetrievalService.Backend.Crawler.Models.Indexing;
using InformationRetrievalService.Backend.Crawler.Models.Store;
using InformationRetrievalService.Backend.Crawler.Models.Store.Entities;
using InformationRetrievalService.Backend.Crawler.PoolManager;
using InformationRetrievalService.Backend.Crawler.Runner;
using InformationRetrievalService.Backend.Crawler.Store;
using InformationRetrievalService.Backend.Crawler.Store.BinaryStore;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace InformationRetrievalService.Backend.CrawlerAdmin
{
    public partial class Form1 : Form
    {
        private readonly IStore _store;
        private readonly Crawler.Runner.CrawlersRunner _runner;

        public Form1()
        {
            InitializeComponent();
            _store = new BinaryStore();
            Action<string, IndexingResultMetrics> logAction = WriteLog;
            _runner = new CrawlersRunner(_store, logAction);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var metrics = _store.GetStoreMetrics();
            labelDocumentCount.Text = metrics.DocumentCount.ToString();
            labelTermsCount.Text = metrics.TermsCount.ToString();
            labelLinksCount.Text = metrics.LinksCount.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //IStore store = new BinaryStore();
            //store.RankingQuery("архитектура процессоров");
            var queryTerminalForm = new QueryTerminal();
            queryTerminalForm.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            var f = new StoreBrowser();
            f.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int count = Convert.ToInt32(numericUpDown1.Text);
            _runner.Run(count);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            _runner.StopAll();
        }

        private void WriteLog(string message, IndexingResultMetrics metrics=null)
        {
            //richTextBox1.Text += $@"{message} {Environment.NewLine}";

            richTextBox1.Invoke((MethodInvoker)delegate
            {
                richTextBox1.Text += $@"{message} {Environment.NewLine}";
            });

            if (metrics != null)
            {
                var nfi = (NumberFormatInfo)CultureInfo.InvariantCulture.NumberFormat.Clone();
                nfi.NumberGroupSeparator = " ";

                labelDocumentCount.Invoke((MethodInvoker) delegate { labelDocumentCount.Text = (Convert.ToInt32(labelDocumentCount.Text) + 1).ToString(); });
                labelTermsCount.Invoke((MethodInvoker)delegate { labelTermsCount.Text = (Convert.ToInt32(labelTermsCount.Text) + metrics.TermCount).ToString(nfi); });
                labelLinksCount.Invoke((MethodInvoker)delegate { labelLinksCount.Text = (Convert.ToInt32(labelLinksCount.Text) + metrics.LinksCount).ToString(nfi); });
            }
        }
    }
}
