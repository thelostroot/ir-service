﻿using System;
using System.Collections.Generic;
using System.Linq;
using InformationRetrievalService.Backend.Crawler.Models.Store.Entities;
using InformationRetrievalService.Backend.Crawler.Models.Store.Retrieval;

namespace InformationRetrievalService.Backend.RetrievalAlgorithms
{
    public static class VectorModel
    {
        public static double GetTf(double termCount, int termInDocument)
        {
            return termCount / termInDocument;
        }

        // TODO sumary
        public static double GetTfIdf(double tf, long documentFreq, long allDocumentsCount)
        {
            double idf = Math.Log((double)allDocumentsCount / documentFreq);
            return tf * idf;
        }

        public static List<QueryResult> CosineSimilarity(Document queryDocument, List<Document> resultDocuments)
        {
            var result = new List<QueryResult>();

            // Cчитаем евклидовую норму для документа из запроса
            double queryDocumentBottomSum = Math.Sqrt(queryDocument.Terms.Sum(x => Math.Pow(x.TfIdf, 2)));

            foreach (var resultDocument in resultDocuments)
            {
                double top = 0;
                for (int i = 0; i < queryDocument.Terms.Count; i++)
                {
                    top += queryDocument.Terms[i].TfIdf * resultDocument.Terms[i].TfIdf;
                }

                // Cчитаем евклидовую норму для документа из результатов
                double resultDocumentBottomSum = Math.Sqrt(resultDocument.Terms.Sum(x => Math.Pow(x.TfIdf, 2)));

                double bottom = queryDocumentBottomSum * resultDocumentBottomSum;

                double cos = top / bottom;

                result.Add(new QueryResult()
                {
                    Cos = cos,
                    Document = resultDocument
                });
            }

            return result;
        }
    }
}
